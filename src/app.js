'use strict';

// requirements
const express = require('express');
const PositiveAmount = require('./PositiveAmount');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    // Preferably should isntantiate PositiveAmount object here, but can't due to
    // natute of TOCTOU test case 'transfer(1000,paylod)'
    const startingBalance = 1000;
    const transferAmount = req.query.amount;

    try {
        const { balance, transfered } = transfer(
            startingBalance,
            transferAmount
        );
        return res
            .status(200)
            .end(
                `Successfully transfered: ${transfered.toString()}. ` +
                    `Your balance: ${balance.toString()}`
            );
    } catch (e) {
        return res
            .status(400)
            .end(
                'Transfer failed. Either insufficient funds or negative transfer amount.' +
                    `Your balance: ${startingBalance.toString()}`
            );
    }
});

// Transfer amount service
var transfer = (_balance, _amount) => {
    const startingBalance = PositiveAmount.fromNumber(_balance);
    const transferAmount = PositiveAmount.fromString(_amount.toString());

    const balance = startingBalance.subtract(transferAmount).value;
    const transfered = transferAmount.value;

        return { balance, transfered };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
