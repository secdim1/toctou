const MAX_INT_32_BIT = 2147483647;

// Can be 0
class PositiveAmount {
    // Cannot be negative
    constructor(value) {
        this.value = value;

        // Deep freeze?
        Object.freeze(this);
    }

    subtract(amount) {
        const newValue = this.value - amount.value;

        return PositiveAmount.fromNumber(newValue);
    }

    toString() {
        return this.value.toString();
    }

    static fromString(value) {
        if (value === null) {
            throw new TypeError('Value is null');
        }

        if (value.length > 10 || value.length < 1) {
            throw new RangeError('Value does not meet size spec');
        }

        if (typeof value === 'number') {
            return PositiveAmount.fromNumber(Number(value));
        } else if (typeof value !== 'string') {
            throw new TypeError('Value is not a string');
        }

        return PositiveAmount.fromNumber(Number(value));
    }

    static fromNumber(value) {
        if (value === null) {
            throw new TypeError('Value is null');
        }

        if (value >= MAX_INT_32_BIT || value < 0) {
            throw new RangeError('Value is out of range');
        }

        if (typeof value === 'string') {
            return PositiveAmount.fromString(value);
        } else if (isNaN(value)) {
            throw new TypeError('Value is NaN');
        }

        if (!Number.isInteger(value)) {
            throw new RangeError('Value is not an integer');
        }

        return new PositiveAmount(value);
    }
}

module.exports = PositiveAmount;
